---
layout: 2020/post
section: proposals
category: talks
title: Trabajando en remoto, caos y destrucción
---

Breve introducción sobre cómo funcionan los equipos remotos, desde el punto de vista de una jefa de proyecto y una desarrolladora.

## Formato de la propuesta

Indicar uno de estos:

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

La mayoría de proyectos de software libre funcionan de forma distribuida, que es algo que a lo que la mayoría de las personas que trabajan en informática no están acostumbradas. ¿Cómo se puede dar este salto? ¿Es realmente tan caótico como se dice? ¿O quizás, más bien, sea el futuro de la informática?

A lo largo de la charla romperemos con mitos, daremos consejos y contaremos cómo es el día a día cuando tu equipo está distribuido en diferentes localizaciones.

## Público objetivo

Todos los interesados en cómo funciona un equipo distribuido.

## Ponente(s)

-   **Rocío Berenguel**: Ingeniera informática de nacimiento, actualmente team leader en Oesía. Amante de lo técnico y del trabajo en equipo.

-   **María Arias de Reyna**: Senior Software Engineer en Red Hat. María es una vieja conocida en comunidades y eventos geoespaciales. Ahora también está peleando con Middleware.

### Contacto(s)

-   **María Arias de Reyna Domínguez**: delawen at gmail dot com

## Comentarios

Ninguno.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/2020/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
