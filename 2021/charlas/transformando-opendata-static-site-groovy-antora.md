---
layout: 2021/post
section: proposals
category: talks
author: Jorge Aguilera
title: Transformando OpenData en un Static Site con Groovy y Antora
---

En esta charla corta voy a explicar un sencillo script en Groovy que vuelca diferentes datasets #OpenData con la información de las fuentes públicas (de beber) de varias ciudades y los convierte en un static site con posibilidad de Geolocalización usando Antora


## Formato de la propuesta

Indicar uno de estos:
-   [x]  Charla corta (10 minutos)
-   [ ]  Charla (25 minutos)

## Descripción

En esta charla corta voy a explicar un sencillo script en Groovy que vuelca diferentes datasets #OpenData con la información de las fuentes públicas (de beber) de varias ciudades y los convierte en un static site con posibilidad de Geolocalización usando Antora.

El objetivo de la charla no es tanto el script ni Antora sino tal vez servir de inspiración para otros tipos de proyectos donde tenemos datos semiestructurados y no sabemos cómo ofrecerlos a los usuarios.

Esta forma la he empleado para generar un site con los precios de las gasolineras y otro con las farmacias de guardia de Granada.

-   Web del proyecto: <https://fuentes.netlify.app>

## Público objetivo

Todo el mundo.

## Ponente(s)

Jorge Aguilera, programador.

### Contacto(s)

-   Nombre: Jorge Aguilera
-   Email: jorge.aguilera@puravida-software.com
-   Web personal: <https://jorge.aguilera.soy>
-   Mastodon (u otras redes sociales libres): <https://mastodon.madrid/web/accounts/5163>
-   Twitter: <https://twitter.com/jagedn>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios



## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
