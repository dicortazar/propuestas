---
layout: 2021/post
section: proposals
category: talks
author: Mario García
title: Entornos de Desarrollo con Vagrant
---

Imagina que estás creando una aplicación y necesitas replicar el entorno en otro equipo de cómputo, en especial si trabajas en un equipo de desarrollo. Una forma de hacerlo es instalar y configurar cada herramienta siguiendo las instrucciones para el sistema operativo, y hacer esto cada vez que lo requieras. Una mejor solución sería usar Vagrant, una herramienta que puede ayudarte a configurar entornos de desarrollo de manera sencilla, los cuales puedes replicar sin mucho esfuerzo.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Imagina que estás creando una aplicación y necesitas replicar el entorno en otro equipo de cómputo, en especial si trabajas en un equipo de desarrollo. Una forma de hacerlo es instalar y configurar cada herramienta siguiendo las instrucciones para el sistema operativo, y hacer esto cada vez que lo requieras. Una mejor solución sería usar Vagrant, una herramienta que puede ayudarte a configurar entornos de desarrollo de manera sencilla y práctica, los cuales puedes replicar sin mucho esfuerzo.

Vagrant funciona con VirtualBox, Docker, entre otros provedores. La instalación de paquetes puede realizarse usando scripts de bash o herramientas como Ansible, Puppet o Chef. ¿Cómo instalar y configurar tus entornos de desarrollo? En esta charla te contaré como preparar tus entornos virtuales, las ventajas de usar Vagrant y algunas consideraciones a tener en cuenta.


-   Web del proyecto: <https://www.vagrantup.com/>

## Público objetivo

Desarrolladores
Usuarios de GNU/Linux

## Ponente(s)

Mario ha sido usuario y contribuidor activo en proyectos de Software Libre y Código Abierto por más de una década. Desarrollador web con experiencia en tecnologías backend incluyendo Python y Rust. Conferencista en eventos de tecnología e innovación desde 2008. Es miembro de programas globales de difusión de tecnología como GitLab Heroes, GitKraken Ambassadors y Hashicorp Ambassadors. Escribe artículos técnicos en DEV y PuntoTech

### Contacto(s)

-   Nombre: Mario García
-   Email: hi@mariog.xyz
-   Web personal: <https://mariog.xyz>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/mariogmd>
-   GitLab: <https://gitlab.com/mattdark>
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios



## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
