---
layout: 2021/post
section: proposals
category: talks
author: Samuel Iglesias Gonsálvez
title: Introducción a Mesa
---

Hoy en día, hay un gran número de aplicaciones que necesitan aceleración gráfica por hardware en cualquier distribución moderna de GNU/Linux. No sólamente juegos, pero también entornos de escritorio, herramientas ofimáticas o incluso navegadores, entre otros. La aceleración gráfica requiere un driver que soporte el HW presente en el sistema.

Esta charla dará una pequeña introducción a Mesa, la libreria que contiene los drivers libres de  OpenGL y Vulkan.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Hoy en día, hay un gran número de aplicaciones que necesitan aceleración gráfica por hardware en cualquier distribución moderna de GNU/Linux. No sólamente juegos, pero también entornos de escritorio, herramientas ofimáticas o incluso navegadores, entre otros. La aceleración gráfica requiere un driver que soporte el HW presente en el sistema.

Esta charla dará una pequeña introducción a Mesa, la libreria que contiene los drivers libres de  OpenGL y Vulkan para una gran variedad de tarjetas gráficas.

La charla cubrirá aspectos como qué drivers hay en Mesa, cómo se organiza la comunidad, quién compone la comunidad y qué podemos hacer para contribuir a este magnífico proyecto de software libre.

-   Web del proyecto: <https://www.mesa3d.org/>

## Público objetivo

Cualquier usuario interesado en conocer más Mesa y cómo interactua con el resto del stack gráfico de GNU/Linux y los desarrolladores que quieran empezar a contribuir a los drivers libres que proporciona Mesa.

## Ponente(s)

Samuel Iglesias es un ingeniero de telecomunicación por la Universidad de Oviedo. Trabaja actualmente como parte del equipo de Gráficos de Igalia, una consultora gallega enfocada en proyectos open-source. Samuel ha estado contribuyendo durante 7 años en distintos drivers en Mesa así como en las testing suites open-source usadas para depurar los drivers (Piglit, Khronos CTS. Además, es miembro de la junta directiva de X.Org Foundation desde el año 2019.

### Contacto(s)

-   Nombre: Samuel Iglesias Gonsálvez
-   Email: siglesias@igalia.com
-   Web personal: <https://www.samuelig.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@samuelig>
-   Twitter: <https://twitter.com/samuelig>
-   GitLab: <https://gitlab.com/samuelig>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/samuelig/>

## Comentarios



## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
