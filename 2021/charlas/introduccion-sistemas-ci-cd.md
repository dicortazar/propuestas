---
layout: 2021/post
section: proposals
category: talks
author: Rafael Aybar Segura
title: Introducción a los sistemas de CI/CD
---

Esta charla está destinada a cualquier persona/empresa que quiera entender una parte del mundillo del CI/CD y entender sus consecuencias.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Esta charla está destinada a cualquier persona/empresa que quiera entender una parte del mundillo del CI/CD y entender sus consecuencias.

Se utilizará el tiempo del que se disponga para presentar ejemplos sobre prácticas combinadas de integración continua y entrega continua usando un entorno dockerizado con Jenkins.

-   Web del proyecto: <https://www.jenkins.io/>

## Público objetivo

Se recomienda nociones de desarrollo y/o sistemas básicos.

## Ponente(s)

Rafael Aybar Segura,  técnico superior de Administración de Sistemas Informáticos en Red que aplica metodologías DevOps.

### Contacto(s)

-   Nombre: Rafael Aybar Segura
-   Email:
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios



## Preferencias de privacidad

-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [ ]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
