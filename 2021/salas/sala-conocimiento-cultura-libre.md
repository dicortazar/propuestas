---
layout: 2021/post
section: proposals
category: devrooms
community: Wikimedia España
title: Sala sobre Conocimiento y Cultura Libre
---

## Descripción de la sala

La cultura y el conocimiento libres sustentan la idea de bienes comunes del conocimiento y el derecho de todas las personas a participar en la vida cultural. Desde Wikimedia España defendemos los proyectos Wikimedia y también los del resto de agentes de cultura libre como bienes comunes dentro de una Internet libre y abierta.

El propósito de esta sala es generar un debate y punto de encuentro dentro de las comunidades libres para tratar dos temas de candente actualidad: los derechos de autoría en el ecosistema digital y la lucha contra la desinformación.

El objetivo explorar juntas nuevas vías para defender el derecho a compartir conocimiento verificable y cultura en línea.

La sala se plantea como punto de encuentro y altavoz para activistas por los derechos digitales, periodistas, personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, paticipantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

Se pondrá el foco en revisar y debatir políticas o prácticas que impulsen y protejan la cultura y el conocimiento libre y a quienes participan en su construcción colectiva.

## Comunidad que la propone

#### Wikimedia España

Wikimedia España es el capítulo reconocido por la Fundación Wikimedia para operar en el Estado español. Somos una asociación sin ánimo de lucro y reconocida como entidad de utilidad pública que promueve el conocimiento libre y los proyectos Wikimedia, siendo Wikipedia el más conocido. Nos constituimos en 2011 y llevamos todos estos años trabajando para mejorar la accesibilidad, el uso y la participación en Wikipedia y sus proyectos hermanos tanto a nivel social como institucional.

La visión del movimiento Wikimedia es conseguir un mundo en el que todas las personas tengan acceso libre a la suma del conocimiento y puedan participar en su construcción colectiva. Seguimos las filosofías del conocimiento y la cultura libres, que defienden el derecho fundamental de acceso a estos, y todos nuestros proyectos operan sobre plataformas de software libre.

Wikipedia, la enciclopedia que todo el mundo puede editar, es nuestro proyecto más conocido, pero trabajamos con otros 12 proyectos colaborativos de conocimiento libre en la red. Por ejemplo, Wikidata, una base de datos estructurada; o Wikimedia Commons, el repositorio multimedia libre más grande del mundo.

Todo el mundo tiene conocimiento para compartir. Desde escribir o mejorar un artículo en Wikipedia, a documentar manifestaciones culturales y subir las imágenes a Wikimedia Commons. Personas individuales, colectivos u organizaciones: todas pueden formar parte del ecosistema Wikimedia.

-   Web de la comunidad: <https://www.wikimedia.es/>
-   Mastodon (u otras redes sociales libres):
-   Twitter: [@wikimedia_es](https://twitter.com/wikimedia_es)
-   Gitlab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

### Contacto(s)

-   Nombre de contacto: Virginia Díez
-   Email de contacto:
    -   Para organización: <virginiadiez@wikimedia.es>
    -   Para contacto público de sala: <info@wikimedia.es>

## Público objetivo

Activistas por los derechos digitales, periodistas, personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, paticipantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

## Formato

- 11:00 a 12:00 - Panel sobre derechos de autoría en el ecosistema digital: balence de la transposición de la DEMUD al marco jurídico español en términos de cultura libre y acceso al conocimiento

- 14:30 a 15:30 - I Wikimedia Movement Meeting on Anti-Desinformation: How to fight desinformation from Wikipedia and its sister projects

- 16:00 a 18:00 - Verificatón: sesión de trabajo para analizar y mejorar la información libre disponible en Wikipedia sobre la COVID-19 en tiempos de infodemia

## Preferencia horaria

-   Duración: 1 día
-   Día: Viernes 25 de junio

## Comentarios

¡Ganas de leer vuestros comentarios sobre la propuesta y de adaptarnos a las necesidades de la conferencia!

## Preferencias de privacidad

-   [ ]  Damos permiso para que nuestro email de contacto sea publicado con la información de la sala.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Aceptamos coordinarnos con la organización de esLibre para la organización de la sala.
-   [x]  Aceptamos que la sala puede ser retirada si no hay un programa terminado para la fecha decidida por la organización.
-   [x]  Confirmamos que al menos una persona de entre las que proponen la sala estará conectada el día programado para realizarla.
