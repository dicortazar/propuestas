[There is an [English version](README-en.md) of this document]

# Envío de propuestas para esLibre 2021

_¿Conoces a alguien que podría querer hablar en el congreso? Envíale esta página a quien quieras, te lo agradeceremos un montón si nos ayudas a llegar a más gente._

El [congreso esLibre](https://eslib.re) es un encuentro de _personas interesadas en las [tecnologías libres](https://www.gnu.org/philosophy/free-sw.es.html) y la [cultura libre](https://es.wikipedia.org/wiki/Cultura_libre), enfocado a compartir conocimiento y experiencias alrededor de las mismas_. La próxima edición volverá a ser retransmitida de forma online los días viernes 25 y sábado 26 de junio, siendo en esta ocasión la comunidad anfitriona **[LibreLabUCM](https://librelabucm.org/)**, contando también con el apoyo de las comunidades que ya organizaron las ediciones pasadas: **[Interferencias](https://interferencias.tech/)**, **[LibreLabGRX](https://librelabgrx.cc)** y la **[OfiLibre de la URJC](https://ofilibre.gitlab.io/)**.

En esLibre intentamos mantener un evento anual donde todo el mundo está invitado a participar, lo único que necesita tener algo que decir sobre el mundo de las tecnologías y cultura libre. Nos interesan personas de todas las edades, experiencias y perfiles ya sean técnicas o usuarias. Estamos deseando escuchar y aprender de todas las personas y nuestro objetivo principal es llegar a comunidades de todo tipo.

### Temática

El **software libre** es la temática central de **esLibre**, pero lo que realmente queremos es conseguir un evento donde encontremos tanto actividades técnicas como otras de un nivel más divulgativo, o incluso que den pie a debates para compartir diferentes puntos de vista sobre aspectos del mundo del conocimiento libre. Esto puede comprender comentarios sobre licencias de publicación, aplicaciones y desarrollo de diversas tecnologias, intereses e inquietudes que quieran presentar diferentes comunidades, o la exploración y divulgación del arte libre.

En líneas generales, _temas relacionados directamente con obras (software, hardware, cultura, etc.) que se distribuyan con licencias libres_, entendiendo las mismas según la [definición de la FSF de software libre](https://www.gnu.org/philosophy/free-sw.html) o la [definición de OSI de open source](https://opensource.org/osd); o en un ámbito más divulgativo, que se entienda que cumplan la [definición de obra cultural libre](https://freedomdefined.org/Definition/Es): todo esto es lo que se considera que encajan perfectamente en **esLibre**. ¿Tienes algo en mente? ¡Anímate!

Aunque mucho software libre (y en general obras libres) se distribuye comercialmente sin que esto presente problema alguno, queremos mantener esLibre como un foro neutro con respecto a los intereses comerciales. En general, no se aceptarán charlas, talleres ni ningún otro tipo de actividad que promocionen específicamente una oferta comercial.

### Tipos de propuestas

Hay varios tipos de contribuciones que puedes proponer (pero no solo):

-   **Charlas**. Presentación "tradicional", en formato normal (25 min. aproximadamente) o en formato relámpago (10 min. aproximadamente). La organización podrá proponer cambios de formato a alguna de las charlas, por motivos organizativos o de contenidos. También podrá, de forma excepcional, proponer a alguna charla un formato más largo.

-   **Talleres**. Presentación práctica, "manos en la masa". Puede ser en muchos formatos, desde demostraciones donde las personas que asistan puedan seguir una especie de "paseo guiado" por la temática del taller, hasta sesiones de iniciación a una tecnología donde se pueda experimentar con ella; en general, cualquier formato práctico que puedas considerar que sea interesante para dar a conocer algo.

-   **Salas (que quizas te suenen como devroom)**. También se puede proponer el programa de una sala, que se realizaría en paralelo con el resto de actividades del congreso. Normalmente una sala estará organizada por y/o para una comunidad, y habitualmente será especializada en un tema (aunque también puede ser generalista). Sólo como ejemplo, en ediciones anteriores tuvimos salas que trataban temática más generales como el software libre en la Universidad, privacidad y derechos digitales o la divulgación de la cultura libre; y otras más específicas como Perl/Raku, GNOME, la programación funcional o tecnologías para el fomento de la lengua andaluza.

-   **Sección de artículos**. Puedes considerar esto como una sección de "pósters virtuales", donde dedicaremos un apartado de la web a exponer todas aquellas publicaciones que hayas realizado y que te gustaría que las personas pudieran conocer, dando si quieres un medio de contacto por el que te pudieran plantear cuestiones o simples comentarios. Además, se aprovecharía para anunciarlos en los descansos del congreso.

-   **Tablón de proyectos**. Si hay un proyecto de software libre de cualquier ámbito sobre el que te gustaría que se hiciera eco, pero prefieres que sea de una forma más informal o sin tener que asumir el foco de atención como en los tipos de actividades anteriores, también puedes proponerlo y pondremos los medios para reunir gente que pueda estar interesada en el mismo proyecto.

-   **Otras actividades**. ¡Usa tu imaginación! Propón otros formatos, tenemos mucho interés en explorar otras formas de compartir conocimiento (y más ahora que no nos separan barreras físicas 🐧).

### Me interesa impartir una charla pero... ¿cómo?

¿Nunca has impartido una charla pero quieres estrenarte? En este evento nos encantaría acompañarte en esa aventura. Somos conscientes de que dar una charla puede ser un reto importante y por eso queremos darte una serie de pautas que te ayuden a exponer aquí (o donde sea). Te dejamos una lista de recursos para exponer sin usar un "powerpoint" (🙃): [Markdown](https://markdown.es/sintaxis-markdown/), [LaTeX](https://es.wikibooks.org/wiki/Manual_de_LaTeX/Texto_completo), [Marp](https://www.genbeta.com/herramientas/marp-herramienta-que-nos-permite-crear-presentaciones-modo-texto-usando-markdown), [Pandoc](https://ondiz.github.io/cursoLatex/Contenido/15.Pandoc.html)...

-   La gente de TED ha creado una [lista de vídeos](https://www.ted.com/playlists/574/how_to_make_a_great_presentation) que enseñan a hacer presentaciones.

-   Al ser un congreso online, necesitarás como mínimo un micró, una webcam y el ordenador (o un móvil en última instancia). Puedes prácticar a hablar frente al ordenador llamando a tu familia o amistades por [Jitsi](https://meet.jit.si/) y será casi igual que cuando lo hagas de verdad.

-   No escribas mucho texto en la presentación, haz una lista de puntos claves y desarróllalos hablando. Como es online, puedes tener una chuleta y nadie se dara cuenta (😉), pero procura no leer todo el tiempo o no quedará igual de fluido. ¡Dejate llevar! Piensa que estas en una conversación con alguien cercano.

-   Para que las y los asistentes que te escuhen puedan seguirte mejor, crea un índice exponiendo las ideas de las que quieres hablar y sigue ese orden durante la charla.

-   Acaba con un resumen que deje en las personas que te están escuchando con una idea que llevarse a sus vidas, así podrás crear un impacto mucho más interesante.

De nuevo, todo esto son sugerencias. Crea tu presentación como quieras y si necesitas más guía y ayuda no dudes en preguntar por ella a [la organización](mailto:propuestas@eslib.re). Para inspirarte, te dejamos una lista de charlas online que puedes ver ahora mismo:

-   [Miriam González: Sistema de diseño para dummies (TotoConf)](https://www.youtube.com/watch?v=ht6-jX8YF38)
-   [Ana Valdivia: Cómo aplicar el feminismo a los datos (TotoConf)](https://www.youtube.com/watch?v=VGfoq5WO0Kc)
-   [Erika Heidi: The art of programming (Codeland)](https://www.youtube.com/watch?v=1snO9k2gOu4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=6)
-   [Joe Karlsson: An introduction to IoT](https://www.youtube.com/watch?v=zHvrtt5raA4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=13)

Ahí fuera hay un montón de proyectos interesantes libres en los que participar, o usar. Puedes empezar [buscando un sistema operativo libre](https://distrowatch.com/) que vaya acorde con tu personalidad, leer algunos proyectos de desarrollo web en un [blog libre](https://dev.to/) o de metasoftware-libre en [este otro](http://www.elbinario.net/). También puedes explorar las diferentes redes sociales libres del [Fediverso](https://fediverse.party/) como [Mastodon](https://joinmastodon.org/). Date un paseo por estos proyectos y si te gustan, ven al evento para aprender sobre muchos más.

### Nuestro compromiso por la diversidad

**La organización [se compromete](https://eslib.re/conducta) a crear un ambiente seguro, no tóxico y diverso**, de modo que nos encantaría dar voz a ponentes de las varias comunidades minoritarias que hagan uso o desarrollen software, hardware o cultura libre. Somos conscientes de que hay muchas personas que usan y crean tecnologías libre y apoyan activamente la divulgación de la cultura libre, y muchas veces no son escuchadas, queremos cambiar eso: ofrecemos nuestro micrófono y espacio para que se hable desde estas comunidades.

Queremos aprender y crear puentes, para que la tecnología sea (al fin) un herramientas de apoyo y colaboración. Si crees que en tu comunidad tenéis algo que contar desde el desarrollo o la experiencia usando software libre, háznoslo saber. Buscamos un futuro colaborativo, multidisciplinar y libre, ¡ayúdanos a conseguirlo!

### Fechas

-   Fecha límite de propuestas: **4 de junio**
-   Fecha límite del programa de las salas: **11 de junio** (ese día tendrán que tener su programa finalizado)
-   Publicación del programa definitivo: **11 de junio**
-   Celebración del congreso **esLibre**: **25 y 26 de junio**

**El límite de todas las fechas son a las 23:59 hora en Madrid (CET).**

### Cómo enviar tu propuesta

El envío de propuestas está abierta a todo el mundo, y aunque seguiremos usando **GitLab** como medio para recoger las propuestas por una cuestión de **transparencia y feedback**, también puedes enviar propuestas mediante los formularios que podrás encontrar en los siguientes enlaces:

-   [Propuestas de charlas](https://eslib.re/2021/propuestas/charlas/)
-   [Propuestas de talleres](https://eslib.re/2021/propuestas/talleres)
-   [Propuestas de salas](https://eslib.re/2021/propuestas/salas)
-   [Sección de artículos](https://eslib.re/2021/propuestas/articulos)
-   [Tablón de proyectos](https://eslib.re/2021/propuestas/proyectos)
-   [Otras actividades](https://eslib.re/2021/propuestas/otras)

Todas las propuestas que se envíen mediante esos formularios quedan registradas en nuestro **[repositorio de propuestas](https://gitlab.com/eslibre/propuestas/-/merge_requests)**, y una vez reciban 3 votos positivos por parte de la organización estará oficialmente aceptada. Si fuera necesario aclarar cualquier cuestión o sugerencia también se haría en ese mismo lugar, por lo que no lo pierdas de vista. En cualquier caso, una vez que registres tu propuesta mediante el formulario, te llegará un correo electrónico con la información de la participación registrada y enlace directo a tu propuesta dentro del repositorio.

También puedes enviar tu propuesta haciendo un **[merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) (pull request)** al mismo **[repositorio de propuestas](https://gitlab.com/eslibre/propuestas)**. Para cada tipo de propuesta tienes una plantilla en la carpeta raíz de este repositorio. Cópiala a la carpeta del año en curso (2021 en este caso), dentro de la subcarpeta correspondiente (charlas, salas, etc.). Usa un nombre de fichero que no esté ya usado, y que refleje el título de tu propuesta. Una vez que tengas esto listo, simplemente tienes que hacer hacer un merge request que la incluya y tu propuesta quedará enviada.

Si tienes problemas de cualquier tipo o cualquier tipo de duda, puedes escribirnos a [propuestas@eslib.re](mailto:propuestas@eslib.re).

¡¡¡ANÍMATE A PARTICIPAR!!!
👏👏👏
